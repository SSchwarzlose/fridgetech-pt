﻿//  --------------------------------------------------------------------------------------------------------------------
//  <copyright file="ScreenSelector.cs" company="Sascha Schwarzlose">
//    copyright 2015 by Sascha Schwarzlose
//  </copyright>
//  <summary>
//    TODO
//  </summary>
//  --------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ScreenSelector : MonoBehaviour 
{
    /// <summary>
    /// This method is obsolete due to changes in Unity's API. Use @see:StartLessonName(string sceneName) instead.
    /// </summary>
    /// <param name="level">Index of the Level to load.</param>
    public void StartLesson(int level)
    {
        Application.LoadLevel(level);
    }

    public void StartLessonName(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
